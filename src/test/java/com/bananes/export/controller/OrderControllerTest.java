package com.bananes.export.controller;

import com.bananes.export.model.Order;
import com.bananes.export.model.Recipient;
import com.bananes.export.service.OrderService;
import com.bananes.export.service.RecipientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
class OrderControllerTest {
    public static final String API_ORDER = "/api/order/";
    public static final String ORDER = "order";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrderService orderService;

    @MockBean
    private RecipientService recipientService;

    @Test
    void should_return_set_of_order_when_get_all_order_by_recipient() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Order order = new Order(recipient, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 50);
        doReturn(recipient).when(recipientService).getRecipientById(anyInt());
        doReturn(Set.of(order)).when(orderService).getOrders(recipient);

        // When
        String contentAsString = this.mockMvc.perform(get(API_ORDER + ORDER)
                        .param("recipient-id", "1"))
                .andExpect(status().is(OK.value()))
                .andReturn().getResponse().getContentAsString();
        Set<Order> response = objectMapper.readValue(contentAsString, objectMapper.getTypeFactory().constructCollectionType(Set.class, Order.class));

        // Then
        assertThat(response).isNotEmpty();
    }

    @Test
    void should_return_empty_set_of_order_when_get_all_order_by_recipient() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");

        // When
        String contentAsString = this.mockMvc.perform(get(API_ORDER + ORDER)
                        .param("recipient-id", "1"))
                .andExpect(status().is(OK.value()))
                .andReturn().getResponse().getContentAsString();
        Set response = objectMapper.readValue(contentAsString, Set.class);

        // Then
        assertThat(response).isEmpty();
    }

    @Test
    void should_register_an_order_when_post_order() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Order order = new Order(recipient, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 50);

        // When
        this.mockMvc.perform(post(API_ORDER + ORDER)
                    .content(asJsonString(order))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(ACCEPTED.value()));

        // Then
        verify(orderService).register(any());
    }

    @Test
    void should_delete_order() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Order order = new Order(recipient, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 50);

        // When - Then
        this.mockMvc.perform(delete(API_ORDER + ORDER)
                        .content(asJsonString(order))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(NO_CONTENT.value()));

        // Then
        verify(orderService).remove(any());
    }

    @Test
    void should_update_order() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Order order = new Order(recipient, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 50);
        Order orderUpdate = new Order(recipient, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 500);

        // When - Then
        this.mockMvc.perform(put(API_ORDER + ORDER)
                        .content("[" + asJsonString(order) + ", " + asJsonString(orderUpdate) + "]")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(ACCEPTED.value()));

        // Then
        verify(orderService).updateOrder(any(), any());
    }

    @Test
    void should_return_bad_request_when_modify_wrong_list_of_ORDER() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Order order = new Order(recipient, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 50);

        // When - Then
        this.mockMvc.perform(put(API_ORDER + ORDER)
                        .content("[" + asJsonString(order) + "]")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(BAD_REQUEST.value()));
    }

    private String asJsonString(final Order obj) throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        return mapper.writeValueAsString(obj);
    }
}
