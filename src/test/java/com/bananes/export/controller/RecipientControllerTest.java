package com.bananes.export.controller;

import com.bananes.export.model.Recipient;
import com.bananes.export.service.RecipientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RecipientController.class)
class RecipientControllerTest {
    public static final String API_RECIPIENT = "/api/recipient/";
    public static final String RECIPIENT = "recipient";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RecipientService recipientService;

    @Test
    void should_return_empty_set_when_get_all_recipient() throws Exception {
        // When
        String contentAsString = this.mockMvc.perform(get(API_RECIPIENT + RECIPIENT))
                .andExpect(status().is(OK.value()))
                .andReturn().getResponse().getContentAsString();
        Set response = objectMapper.readValue(contentAsString, Set.class);

        // Then
        assertThat(response).isEmpty();
    }

    @Test
    void should_return_set_of_recipient_when_get_all_recipient() throws Exception {
        // Given
        Recipient recipient1 = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Recipient recipient2 = new Recipient("recipient 2", "address", "75000", "Paris", "France");
        doReturn(Set.of(recipient1, recipient2)).when(recipientService).getAllRecipient();

        // When
        String contentAsString = this.mockMvc.perform(get(API_RECIPIENT + RECIPIENT))
                .andExpect(status().is(OK.value()))
                .andReturn().getResponse().getContentAsString();
        Set<Recipient> response = objectMapper.readValue(contentAsString, objectMapper.getTypeFactory().constructCollectionType(Set.class, Recipient.class));

        // Then
        assertThat(response).isNotEmpty();
        assertThat(response).hasSize(2);
        assertThat(response).contains(recipient1, recipient2);
    }

    @Test
    void should_register_recipient_when_post_recipient() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");

        // When
        this.mockMvc.perform(post(API_RECIPIENT + RECIPIENT)
                        .content(asJsonString(recipient))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(ACCEPTED.value()));

        // Then
        verify(recipientService).save(recipient);
    }

    @Test
    void should_modify_recipient() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Recipient recipientUpdated = new Recipient("recipient 1", "address 2", "75020", "Paris", "France");

        // When
        this.mockMvc.perform(put(API_RECIPIENT + RECIPIENT)
                        .content("[" + asJsonString(recipient) + ", " + asJsonString(recipientUpdated) + "]")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(ACCEPTED.value()));

        // Then
        verify(recipientService).update(recipient, recipientUpdated);
    }

    @Test
    void should_return_bad_request_when_modify_wrong_list_of_recipient() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");

        // When - Then
        this.mockMvc.perform(put(API_RECIPIENT + RECIPIENT)
                        .content("[" + asJsonString(recipient) + "]")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(BAD_REQUEST.value()));
    }

    @Test
    void should_delete_recipient() throws Exception {
        // Given
        Recipient recipient = new Recipient("recipient 1", "address", "75000", "Paris", "France");

        // When - Then
        this.mockMvc.perform(delete(API_RECIPIENT + RECIPIENT)
                        .content(asJsonString(recipient))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(NO_CONTENT.value()));

        // Then
        verify(recipientService).delete(recipient);
    }

    private String asJsonString(final Object obj) throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }
}
