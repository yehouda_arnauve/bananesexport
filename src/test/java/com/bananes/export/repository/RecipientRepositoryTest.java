package com.bananes.export.repository;

import com.bananes.export.model.Recipient;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class RecipientRepositoryTest {
    private RecipientRepository recipientRepository = new RecipientRepository();

    @Test
    void should_get_all_recipient_when_get_all_recipient() {
        Set<Recipient> allRecipient = recipientRepository.getAllRecipient();

        assertThat(allRecipient).isEmpty();
    }

    @Test
    void should_register_recipient_when_add_one_recipient() {
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");

        recipientRepository.save(recipient);
        Set<Recipient> allRecipient = recipientRepository.getAllRecipient();

        assertThat(allRecipient).contains(recipient);
    }

    @Test
    void should_update_recipient_when_change_field() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");
        Recipient recipientUpdate = new Recipient("name", "address", "75020", "Paris", "France");
        recipientRepository.save(recipient);

        // When
        recipientRepository.update(recipient, recipientUpdate);
        Set<Recipient> allRecipient = recipientRepository.getAllRecipient();

        // Then
        assertThat(allRecipient).doesNotContain(recipient);
        assertThat(allRecipient).contains(recipientUpdate);
    }

    @Test
    void should_delete_recipient() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");
        recipientRepository.save(recipient);

        // When
        recipientRepository.delete(recipient);
        Set<Recipient> allRecipient = recipientRepository.getAllRecipient();

        // Then
        assertThat(allRecipient).isEmpty();
    }
}
