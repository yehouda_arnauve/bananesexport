package com.bananes.export.repository;

import com.bananes.export.model.Order;
import com.bananes.export.model.Recipient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class OrderRepositoryTest {

    private OrderRepository orderRepository;

    private Recipient recipient;

    private Order order;

    @BeforeEach
    void initTests() {
        orderRepository = new OrderRepository();
        recipient = new Recipient("name", "address", "75000", "Paris", "France");
        order = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 50);
    }

    @Test
    void should_register_an_order() {
        // When
        orderRepository.register(order);

        // Then
        assertThat(orderRepository.orderSet).hasSize(1);
        assertThat(orderRepository.orderSet).contains(order);
    }

    @Test
    void should_not_register_an_order_twice() {
        // When
        orderRepository.register(order);
        orderRepository.register(order);

        // Then
        assertThat(orderRepository.orderSet).hasSize(1);
        assertThat(orderRepository.orderSet).contains(order);
    }

    @Test
    void should_update_order() {
        // Given
        Order order2 = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 50);

        // When
        orderRepository.update(order, order2);

        // Then
        assertThat(orderRepository.orderSet).hasSize(1);
        assertThat(orderRepository.orderSet).doesNotContain(order);
        assertThat(orderRepository.orderSet).contains(order2);
    }

    @Test
    void should_delete_order() {
        // Given
        Order order2 = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 50);
        orderRepository.register(order);
        orderRepository.register(order2);
        assertThat(orderRepository.orderSet).contains(order);
        assertThat(orderRepository.orderSet).contains(order2);

        // When
        orderRepository.remove(order2);

        // Then
        assertThat(orderRepository.orderSet).hasSize(1);
        assertThat(orderRepository.orderSet).contains(order);
        assertThat(orderRepository.orderSet).doesNotContain(order2);
    }

    @Test
    void should_get_all_order() {
        // Given
        orderRepository.register(order);

        // When
        Set<Order> allOrders = orderRepository.getAllOrders();

        // Then
        assertThat(allOrders).isNotNull().isNotEmpty();
        assertThat(allOrders).hasSize(1);
        assertThat(allOrders).contains(order);
    }

}
