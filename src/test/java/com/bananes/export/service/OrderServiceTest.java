package com.bananes.export.service;

import com.bananes.export.model.Order;
import com.bananes.export.model.Recipient;
import com.bananes.export.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class OrderServiceTest {
    private OrderRepository orderRepository;
    private OrderService orderService;
    private Recipient recipient;
    private Order order;

    @BeforeEach
    void initTests() {
        orderRepository = spy(new OrderRepository());
        orderService = new OrderService(orderRepository, 2.5F, 25, 25);
        recipient = new Recipient("name", "address", "75000", "Paris", "France");
        order = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 50);
    }

    @Test
    void should_register_an_order() {
        // When
        orderService.register(order);

        // Then
        verify(orderRepository).register(order);
        assertThat(orderRepository.getAllOrders()).contains(order);
    }

    @Test
    void should_not_register_order_when_order_exceeded_limits() {
        // Given
        Order orderExceededLimit = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 10_001);

        // When - Then
        assertThrows(IllegalArgumentException.class, () -> orderService.register(orderExceededLimit));
        assertThat(orderRepository.getAllOrders()).doesNotContain(orderExceededLimit);
        verify(orderRepository, never()).register(any());
    }

    @Test
    void should_not_register_order_when_order_under_limits() {
        // Given
        Order orderUnderLimit = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), -1);

        // When - Then
        assertThrows(IllegalArgumentException.class, () -> orderService.register(orderUnderLimit));
        assertThat(orderRepository.getAllOrders()).doesNotContain(orderUnderLimit);
        verify(orderRepository, never()).register(any());
    }

    @Test
    void should_not_register_order_when_order_is_not_divible_by_25() {
        // Given
        Order ordernotDivisibleBy25 = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 24);

        // When - Then
        assertThrows(IllegalArgumentException.class, () -> orderService.register(ordernotDivisibleBy25));
        assertThat(orderRepository.getAllOrders()).doesNotContain(ordernotDivisibleBy25);
        verify(orderRepository, never()).register(any());
    }

    @Test
    void should_register_order_and_calcul_price() {
        // Given
        Order order = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 25);

        //When
        orderService.register(order);

        // Then
        Set<Order> allOrders = orderRepository.getAllOrders();
        assertThat(allOrders).isNotNull().hasSize(1);
        Order order1 = allOrders.stream().findFirst().orElse(null);

        assertThat(order1.getPrice()).isEqualTo(62.5F);
    }

    @Test
    void should_not_register_order_when_delivery_date_is_before_3_weeks() {
        // Given
        Order order = new Order(recipient, ZonedDateTime.now().plus(2, ChronoUnit.WEEKS), 25);

        // When - Then
        assertThrows(IllegalArgumentException.class, () -> orderService.register(order));
        Set<Order> allOrders = orderRepository.getAllOrders();
        assertThat(allOrders).isNotNull().isEmpty();
    }

    @Test
    void should_update_order() {
        // Given
        Order order = new Order(recipient, ZonedDateTime.now().plus(5, ChronoUnit.WEEKS), 25);
        orderService.register(order);
        Order orderUpdate = new Order(recipient, ZonedDateTime.now().plus(12, ChronoUnit.WEEKS), 250);

        // When - Then
        orderService.updateOrder(order, orderUpdate);
        Set<Order> allOrders = orderRepository.getAllOrders();
        assertThat(allOrders).contains(orderUpdate);
        assertThat(allOrders).doesNotContain(order);
    }

    @Test
    void should_not_update_order_when_delivery_date_is_before_3_weeks() {
        // Given
        Order order = new Order(recipient, ZonedDateTime.now().plus(5, ChronoUnit.WEEKS), 25);
        orderService.register(order);
        Order orderUpdate = new Order(recipient, ZonedDateTime.now().plus(2, ChronoUnit.WEEKS), 25);

        // When - Then
        assertThrows(IllegalArgumentException.class, () -> orderService.updateOrder(order, orderUpdate));
        Set<Order> allOrders = orderRepository.getAllOrders();
        assertThat(allOrders).isNotNull().isNotEmpty();
        assertThat(allOrders).contains(order);
        assertThat(allOrders).doesNotContain(orderUpdate);
    }

    @Test
    void should_not_update_order_when_order_is_not_divible_by_25() {
        // Given
        Order order = new Order(recipient, ZonedDateTime.now().plus(5, ChronoUnit.WEEKS), 25);
        orderService.register(order);
        Order ordernotDivisibleBy25 = new Order(recipient, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 24);

        // When - Then
        assertThrows(IllegalArgumentException.class, () -> orderService.updateOrder(order, ordernotDivisibleBy25));
        assertThat(orderRepository.getAllOrders()).doesNotContain(ordernotDivisibleBy25);
        assertThat(orderRepository.getAllOrders()).contains(order);
        verify(orderRepository, never()).update(any(), any());
    }

    @Test
    void should_delete_order() {
        // Given
        orderService.register(order);

        // When
        orderService.remove(order);

        // Then
        Set<Order> allOrders = orderRepository.getAllOrders();
        assertThat(allOrders).isEmpty();
    }

    @Test
    void should_get_order_by_recipient_when_get_orders() {
        // Given
        // Recipient 1
        Recipient recipient1 = new Recipient("recipient 1", "address", "75000", "Paris", "France");
        Order order1OfRecipient1 = new Order(recipient1, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 50);
        Order order2OfRecipient1 = new Order(recipient1, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 1050);
        Order order3OfRecipient1 = new Order(recipient1, ZonedDateTime.now().plus(5, ChronoUnit.WEEKS), 50);
        orderService.register(order1OfRecipient1);
        orderService.register(order2OfRecipient1);
        orderService.register(order3OfRecipient1);

        // Recipient 2
        Recipient recipient2 = new Recipient("recipient 2", "address", "75000", "Paris", "France");
        Order order1OfRecipient2 = new Order(recipient2, ZonedDateTime.now().plus(3, ChronoUnit.WEEKS), 150);
        Order order2OfRecipient2 = new Order(recipient2, ZonedDateTime.now().plus(4, ChronoUnit.WEEKS), 100);
        Order order3OfRecipient2 = new Order(recipient2, ZonedDateTime.now().plus(5, ChronoUnit.WEEKS), 50);
        orderService.register(order1OfRecipient2);
        orderService.register(order2OfRecipient2);
        orderService.register(order3OfRecipient2);

        // When
        Set<Order> orders = orderService.getOrders(recipient2);

        // Then
        assertThat(orders).isNotNull().isNotEmpty();
        assertThat(orders).hasSize(3);
        assertThat(orders.stream().findFirst().get().getRecipient()).isEqualTo(recipient2);
    }

}
