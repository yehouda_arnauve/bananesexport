package com.bananes.export.service;

import com.bananes.export.model.Recipient;
import com.bananes.export.repository.RecipientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class RecipientServiceTest {
    private RecipientRepository recipientRepository;

    private RecipientService recipientService;

    @BeforeEach
    void initTests() {
        recipientRepository = spy(new RecipientRepository());
        recipientService = new RecipientService(recipientRepository);
    }

    @Test
    void should_get_all_recipient_when_get_all_recipient() {
        // When
        Set<Recipient> allRecipient = recipientService.getAllRecipient();

        // Then
        assertThat(allRecipient).isEmpty();
        verify(recipientRepository).getAllRecipient();
    }

    @Test
    void should_register_recipient_when_add_one_recipient() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");

        // When
        recipientService.save(recipient);
        Set<Recipient> allRecipient = recipientService.getAllRecipient();

        // Then
        assertThat(allRecipient).contains(recipient);
        verify(recipientRepository).save(recipient);
    }

    @Test
    void should_not_register_recipient_when_already_exist() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");

        // When
        recipientService.save(recipient);
        recipientService.save(recipient);
        Set<Recipient> allRecipient = recipientService.getAllRecipient();

        // Then
        assertThat(allRecipient).isNotEmpty();
        assertThat(allRecipient).hasSize(1);
        assertThat(allRecipient).contains(recipient);
        verify(recipientRepository).save(recipient);
    }

    @Test
    void should_update_recipient_when_change_field() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");
        Recipient recipientUpdate = new Recipient("name", "address", "75020", "Paris", "France");
        recipientService.save(recipient);

        // When
        recipientService.update(recipient, recipientUpdate);
        Set<Recipient> allRecipient = recipientService.getAllRecipient();

        // Then
        assertThat(allRecipient).doesNotContain(recipient);
        assertThat(allRecipient).contains(recipientUpdate);
    }

    @Test
    void should_delete_recipient() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");
        recipientService.save(recipient);

        // When
        recipientService.delete(recipient);
        Set<Recipient> allRecipient = recipientService.getAllRecipient();

        // Then
        assertThat(allRecipient).isEmpty();
    }

    @Test
    void should_not_delete_recipient_when_it_does_not_in_data() {
        // Given
        Recipient recipient = new Recipient("name", "address", "75000", "Paris", "France");
        Recipient recipient2 = new Recipient("name2", "address", "75000", "Paris", "France");
        recipientService.save(recipient);

        // When
        recipientService.delete(recipient2);
        Set<Recipient> allRecipient = recipientService.getAllRecipient();

        // Then
        assertThat(allRecipient).isNotEmpty();
        assertThat(allRecipient).contains(recipient);
    }
}
