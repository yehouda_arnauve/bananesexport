package com.bananes.export.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import java.time.ZonedDateTime;

@Data
@EqualsAndHashCode
@ToString
public class Order {
    public Order(@NonNull Recipient recipient, @NonNull ZonedDateTime deliveryDate, @NonNull int quantity) {
        this.recipient = recipient;
        this.deliveryDate = deliveryDate;
        this.quantity = quantity;
    }

    @NonNull
    private Recipient recipient;

    @NonNull
    private ZonedDateTime deliveryDate;

    @NonNull
    private int quantity;

    private float price;
}
