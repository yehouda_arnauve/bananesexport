package com.bananes.export.model;

import lombok.*;

@Data
@EqualsAndHashCode
public class Recipient {
    private int id;
    @NonNull
    private String name;
    @NonNull
    private String address;
    @NonNull
    private String zipCode;
    @NonNull
    private String city;
    @NonNull
    private String country;

    public Recipient(@NonNull String name, @NonNull String address, @NonNull String zipCode, @NonNull String city, @NonNull String country) {
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }
}
