package com.bananes.export.controller;

import com.bananes.export.model.Recipient;
import com.bananes.export.service.RecipientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Slf4j
@Controller
@RequestMapping({ "api/recipient" })
public class RecipientController {
    @Autowired
    private RecipientService recipientService;

    @GetMapping(value = "/recipient")
    @ResponseBody
    public Set<Recipient> getRecipient() {
        log.info("GET - get all recipients");
        return recipientService.getAllRecipient();
    }

    @PostMapping(value = "/recipient")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void saveRecipient(@RequestBody Recipient recipient) {
        log.info("POST - save recipient: {}", recipient);
        recipientService.save(recipient);
    }

    @PutMapping(value = "/recipient")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void saveRecipient(@RequestBody List<Recipient> recipients) {
        if (recipients == null || recipients.size() != 2) {
            throw new IllegalArgumentException("invalide list of recipients");
        }
        log.info("PUT - update recipient: {} to {}", recipients.get(0), recipients.get(1));
        recipientService.update(recipients.get(0), recipients.get(1));
    }

    @DeleteMapping(value = "/recipient")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteRecipient(@RequestBody Recipient recipient) {
        log.info("DELETE - delete recipient: {}", recipient);
        recipientService.delete(recipient);
    }
}
