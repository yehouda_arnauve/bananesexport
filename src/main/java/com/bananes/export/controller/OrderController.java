package com.bananes.export.controller;

import com.bananes.export.model.Order;
import com.bananes.export.model.Recipient;
import com.bananes.export.service.OrderService;
import com.bananes.export.service.RecipientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Slf4j
@Controller
@RequestMapping({ "api/order" })
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private RecipientService recipientService;

    @GetMapping(value = "/order")
    @ResponseBody
    public Set<Order> getOrder(@RequestParam("recipient-id") int recipientId) {
        Recipient recipient = recipientService.getRecipientById(recipientId);
        log.info("GET - get all order for recipient: {}", recipient);
        return orderService.getOrders(recipient);
    }

    @PostMapping(value = "/order")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void saveOrder(@RequestBody Order order) {
        log.info("POST - register order: {}", order);
        orderService.register(order);
    }

    @DeleteMapping(value = "/order")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteOrder(@RequestBody Order order) {
        log.info("DELETE - delete order: {}", order);
        orderService.remove(order);
    }

    @PutMapping(value = "/order")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void saveRecipient(@RequestBody List<Order> orders) {
        if (orders == null || orders.size() != 2) {
            throw new IllegalArgumentException("invalid list of orders");
        }
        log.info("PUT - update recipient: {} to {}", orders.get(0), orders.get(1));
        orderService.updateOrder(orders.get(0), orders.get(1));
    }
}
