package com.bananes.export.repository;

import com.bananes.export.model.Order;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class OrderRepository {
    Set<Order> orderSet = new HashSet<>();

    public void register(Order order) {
        orderSet.add(order);
    }

    public void update(Order order, Order orderUpdate) {
        orderSet.remove(order);
        orderSet.add(orderUpdate);
    }

    public void remove(Order order) {
        orderSet.remove(order);
    }

    public Set<Order> getAllOrders() {
        return orderSet;
    }
}
