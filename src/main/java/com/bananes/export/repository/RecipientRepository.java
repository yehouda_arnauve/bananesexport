package com.bananes.export.repository;

import com.bananes.export.model.Recipient;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class RecipientRepository {
    private Set<Recipient> recipientList = new HashSet<>();

    public Set<Recipient> getAllRecipient() {
        return recipientList;
    }

    public void save(Recipient recipient) throws IllegalArgumentException {
        this.recipientList.add(recipient);
    }

    public void update(Recipient recipient, Recipient update) {
        recipientList.remove(recipient);
        recipientList.add(update);
    }

    public void delete(Recipient recipient) {
        recipientList.remove(recipient);
    }
}
