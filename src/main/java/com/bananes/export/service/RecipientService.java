package com.bananes.export.service;

import com.bananes.export.model.Recipient;
import com.bananes.export.repository.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class RecipientService {
    private RecipientRepository recipientRepository;

    @Autowired
    public RecipientService(RecipientRepository recipientRepository) {
        this.recipientRepository = recipientRepository;
    }

    public Set<Recipient> getAllRecipient() {
        return recipientRepository.getAllRecipient();
    }

    public Recipient getRecipientById(int id) {
        return getAllRecipient().stream().filter(recipient -> recipient.getId() == id).findFirst().orElse(null);
    }

    public void save(Recipient recipient) {
        Set<Recipient> allRecipient = recipientRepository.getAllRecipient();
        if (allRecipient.contains(recipient)) {
            return;
        }
        recipient.setId(allRecipient.size());
        recipientRepository.save(recipient);
    }

    public void update(Recipient recipient, Recipient update) {
        recipientRepository.update(recipient, update);
    }

    public void delete(Recipient recipient) {
        recipientRepository.delete(recipient);
    }
}
