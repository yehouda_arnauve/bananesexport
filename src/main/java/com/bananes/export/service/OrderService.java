package com.bananes.export.service;

import com.bananes.export.model.Order;
import com.bananes.export.model.Recipient;
import com.bananes.export.repository.OrderRepository;
import com.bananes.export.repository.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    private float priceByKg;

    private int kgByBox;

    private int quantityByBox;

    @Autowired
    public OrderService(OrderRepository orderRepository, @Value("${price.by.kg}") float priceByKg, @Value("${kg.by.box}") int kgByBox, @Value("${quantity.by.box}") int quantityByBox) {
        this.orderRepository = orderRepository;
        this.priceByKg = priceByKg;
        this.kgByBox = kgByBox;
        this.quantityByBox = quantityByBox;
    }

    public void register(Order order) throws IllegalArgumentException {
        checkDeliveryDateOfOrder(order.getDeliveryDate());
        checkValidQuantityOfOrder(order.getQuantity());
        calculatePriceForOrder(order);
        this.orderRepository.register(order);
    }

    private void calculatePriceForOrder(Order order) {
        order.setPrice(((order.getQuantity() / quantityByBox) * kgByBox) * priceByKg);
    }

    private void checkValidQuantityOfOrder(int quantity) {
        if (quantity > 10_000) {
            throw new IllegalArgumentException("order's quantity exceed limit");
        }
        if (quantity < 0) {
            throw new IllegalArgumentException("order's quantity is under limit");
        }
        if (quantity % 25 != 0) {
            throw new IllegalArgumentException("order must be by box of 25 bananas");
        }
    }

    private void checkDeliveryDateOfOrder(ZonedDateTime deliveryDate) {
        ZonedDateTime into3Weeks = ZonedDateTime.now().plus(3, ChronoUnit.WEEKS).withHour(0).withMinute(0).withSecond(0);
        if (into3Weeks.isAfter(deliveryDate)) {
            throw new IllegalArgumentException("cannot deliver this order, day of delivery date should be at least: " + into3Weeks.format(DateTimeFormatter.ISO_LOCAL_DATE));
        }
    }

    public void remove(Order order) {
        this.orderRepository.remove(order);
    }

    public Set<Order> getOrders(Recipient recipient) {
        return this.orderRepository.getAllOrders().stream().filter(order -> order.getRecipient().equals(recipient)).collect(Collectors.toSet());
    }

    public void updateOrder(Order order, Order orderUpdate) {
        checkDeliveryDateOfOrder(orderUpdate.getDeliveryDate());
        checkValidQuantityOfOrder(orderUpdate.getQuantity());
        calculatePriceForOrder(orderUpdate);
        orderRepository.update(order, orderUpdate);
    }
}
